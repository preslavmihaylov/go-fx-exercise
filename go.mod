module github.com/preslavmihaylov/go-fx-exercise

go 1.14

require (
	github.com/preslavmihaylov/fxappexample v0.0.0-20200412124103-dbfde3494581
	go.uber.org/fx v1.11.0
	go.uber.org/zap v1.14.1
	gopkg.in/yaml.v2 v2.2.2
)
